import React from "react";
import { StatusBar } from "react-native";
import CombinedStackNavigator from "./src/navigation/CombinedStackNavigator";

const App = () => {
  StatusBar.setHidden(true);

  return <CombinedStackNavigator />;
};

export default App;
