import { Pictogram, PictogramCategory } from "../types/PictogramType";

export const SentenceMaxLenght = 7;

export const shortcuts: Pictogram[] = [
  {
    id: "Peo2",
    word: "ja",
    category: PictogramCategory.People,
    filename: "me",
    custom: false,
  },
  {
    id: "Act2",
    word: "chcieć",
    category: PictogramCategory.Actions,
    filename: "want",
    custom: false,
  },
  {
    id: "Con2",
    word: "czy",
    category: PictogramCategory.Conjunctions,
    filename: "if",
    custom: false,
  },
];
