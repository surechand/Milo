export type Pictogram = {
  id: string;
  word: string;
  category: PictogramCategory;
  filename: string;
  custom: boolean;
};

export enum PictogramCategory {
  Actions = "Działania",
  Adjectives = "Przymiotniki",
  Animals = "Zwierzęta",
  Body = "Części ciała",
  Clothes = "Ciuchy",
  Conjunctions = "Spójniki",
  Feelings = "Uczucia",
  Food = "Jedzenie",
  Home = "Dom",
  Numbers = "Liczby",
  People = "Osoby",
  Places = "Miejsca",
  School = "Szkoła",
  Time = "Czas",
  Transport = "Transport",
}
