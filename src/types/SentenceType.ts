import { Pictogram } from "./PictogramType";

export type Sentence = {
  id: number;
  sentence: Pictogram[];
};
