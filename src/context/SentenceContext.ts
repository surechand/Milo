import { createContext } from "react";
import { Pictogram } from "../types/PictogramType";

interface SentenceInterface {
  sentence: Pictogram[];
  setSentence: React.Dispatch<React.SetStateAction<Pictogram[]>>;
}

export const SentenceContext = createContext<SentenceInterface>(
  {} as SentenceInterface
);
