import { createContext } from "react";
import { Sentence } from "../types/SentenceType";

interface ListInterface {
  favouritesList: Sentence[];
  setFavouritesList: React.Dispatch<React.SetStateAction<Sentence[]>>;
}

export const ListContext = createContext<ListInterface>({} as ListInterface);
