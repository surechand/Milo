import { Pictogram, PictogramCategory } from "../../types/PictogramType";

const getFeelingsData: Pictogram[] = [
  {
    id: "Fee1",
    word: "szczęście",
    category: PictogramCategory.Feelings,
    filename: "happiness",
    custom: false,
  },
  {
    id: "Fee2",
    word: "śmiech",
    category: PictogramCategory.Feelings,
    filename: "laugh",
    custom: false,
  },
  {
    id: "Fee3",
    word: "smutek",
    category: PictogramCategory.Feelings,
    filename: "sadness",
    custom: false,
  },
];

export default getFeelingsData;
