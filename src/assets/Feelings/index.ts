const getFeelings = (filename: string) => {
  switch (filename) {
    case "happiness": {
      return require("./happiness.png");
    }
    case "laugh": {
      return require("./laugh.png");
    }
    case "sadness": {
      return require("./sadness.png");
    }
    default: {
      return require("./../icon.png");
    }
  }
};

export default getFeelings;
