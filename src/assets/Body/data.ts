import { Pictogram, PictogramCategory } from "../../types/PictogramType";

const getBodyData: Pictogram[] = [
  {
    id: "Bod1",
    word: "ręka",
    category: PictogramCategory.Body,
    filename: "arm",
    custom: false,
  },
  {
    id: "Bod2",
    word: "twarz",
    category: PictogramCategory.Body,
    filename: "face",
    custom: false,
  },
  {
    id: "Bod3",
    word: "noga",
    category: PictogramCategory.Body,
    filename: "leg",
    custom: false,
  },
];

export default getBodyData;
