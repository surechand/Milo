const getBody = (filename: string) => {
  switch (filename) {
    case "arm": {
      return require("./arm.png");
    }
    case "face": {
      return require("./face.png");
    }
    case "leg": {
      return require("./leg.png");
    }
    default: {
      return require("./../icon.png");
    }
  }
};

export default getBody;
