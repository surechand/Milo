import { Pictogram, PictogramCategory } from "../../types/PictogramType";

const getPeopleData: Pictogram[] = [
  {
    id: "Peo1",
    word: "tata",
    category: PictogramCategory.People,
    filename: "father",
    custom: false,
  },
  {
    id: "Peo2",
    word: "ja",
    category: PictogramCategory.People,
    filename: "me",
    custom: false,
  },
  {
    id: "Peo3",
    word: "mama",
    category: PictogramCategory.People,
    filename: "mother",
    custom: false,
  },
  {
    id: "Peo4",
    word: "ty",
    category: PictogramCategory.People,
    filename: "you",
    custom: false,
  },
];

export default getPeopleData;
