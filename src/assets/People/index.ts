const getPeople = (filename: string) => {
  switch (filename) {
    case "father": {
      return require("./father.png");
    }
    case "me": {
      return require("./me.png");
    }
    case "mother": {
      return require("./mother.png");
    }
    case "you": {
      return require("./you.png");
    }
    default: {
      return require("./../icon.png");
    }
  }
};

export default getPeople;
