const getTransport = (filename: string) => {
  switch (filename) {
    case "airplane": {
      return require("./airplane.png");
    }
    case "bus": {
      return require("./bus.png");
    }
    case "car": {
      return require("./car.png");
    }
    case "ship": {
      return require("./ship.png");
    }
    case "train": {
      return require("./train.png");
    }
    default: {
      return require("./../icon.png");
    }
  }
};

export default getTransport;
