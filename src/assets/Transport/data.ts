import { Pictogram, PictogramCategory } from "../../types/PictogramType";

const getTransportData: Pictogram[] = [
  {
    id: "Tra1",
    word: "samolot",
    category: PictogramCategory.Transport,
    filename: "airplane",
    custom: false,
  },
  {
    id: "Tra2",
    word: "autobus",
    category: PictogramCategory.Transport,
    filename: "bus",
    custom: false,
  },
  {
    id: "Tra3",
    word: "samochów",
    category: PictogramCategory.Transport,
    filename: "car",
    custom: false,
  },
  {
    id: "Tra4",
    word: "statek",
    category: PictogramCategory.Transport,
    filename: "ship",
    custom: false,
  },
  {
    id: "Tra5",
    word: "pociąg",
    category: PictogramCategory.Transport,
    filename: "train",
    custom: false,
  },
];

export default getTransportData;
