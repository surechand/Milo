const getAdjectives = (filename: string) => {
  switch (filename) {
    case "bad": {
      return require("./bad.png");
    }
    case "good": {
      return require("./good.png");
    }
    case "pretty": {
      return require("./pretty.png");
    }
    case "ugly": {
      return require("./ugly.png");
    }
    default: {
      return require("./../icon.png");
    }
  }
};

export default getAdjectives;
