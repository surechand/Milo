import { Pictogram, PictogramCategory } from "../../types/PictogramType";

const getAdjectivesData: Pictogram[] = [
  {
    id: "Adj1",
    word: "zły",
    category: PictogramCategory.Adjectives,
    filename: "bad",
    custom: false,
  },
  {
    id: "Adj2",
    word: "dobry",
    category: PictogramCategory.Adjectives,
    filename: "good",
    custom: false,
  },
  {
    id: "Adj3",
    word: "ładny",
    category: PictogramCategory.Adjectives,
    filename: "pretty",
    custom: false,
  },
  {
    id: "Adj4",
    word: "brzydki",
    category: PictogramCategory.Adjectives,
    filename: "ugly",
    custom: false,
  },
];

export default getAdjectivesData;
