const getFood = (filename: string) => {
  switch (filename) {
    case "breakfast": {
      return require("./breakfast.png");
    }
    case "dinner": {
      return require("./dinner.png");
    }
    case "sandwich": {
      return require("./sandwich.png");
    }
    default: {
      return require("./../icon.png");
    }
  }
};

export default getFood;
