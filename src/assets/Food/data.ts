import { Pictogram, PictogramCategory } from "../../types/PictogramType";

const getFoodData: Pictogram[] = [
  {
    id: "Foo1",
    word: "śniadanie",
    category: PictogramCategory.Food,
    filename: "breakfast",
    custom: false,
  },
  {
    id: "Foo2",
    word: "obiad",
    category: PictogramCategory.Food,
    filename: "dinner",
    custom: false,
  },
  {
    id: "Foo3",
    word: "kanapka",
    category: PictogramCategory.Food,
    filename: "sandwich",
    custom: false,
  },
];

export default getFoodData;
