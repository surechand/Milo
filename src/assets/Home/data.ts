import { Pictogram, PictogramCategory } from "../../types/PictogramType";

const getHomeData: Pictogram[] = [
  {
    id: "Hom1",
    word: "łazienka",
    category: PictogramCategory.Home,
    filename: "bathroom",
    custom: false,
  },
  {
    id: "Hom2",
    word: "sypialnia",
    category: PictogramCategory.Home,
    filename: "bedroom",
    custom: false,
  },
  {
    id: "Hom3",
    word: "kuchnia",
    category: PictogramCategory.Home,
    filename: "kitchen",
    custom: false,
  },
  {
    id: "Hom4",
    word: "salon",
    category: PictogramCategory.Home,
    filename: "livingroom",
    custom: false,
  },
];

export default getHomeData;
