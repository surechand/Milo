const getHome = (filename: string) => {
  switch (filename) {
    case "bathroom": {
      return require("./bathroom.png");
    }
    case "bedroom": {
      return require("./bedroom.png");
    }
    case "kitchen": {
      return require("./kitchen.png");
    }
    case "livingroom": {
      return require("./livingroom.png");
    }
    default: {
      return require("./../icon.png");
    }
  }
};

export default getHome;
