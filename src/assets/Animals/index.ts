const getAnimals = (filename: string) => {
  switch (filename) {
    case "cat": {
      return require("./cat.png");
    }
    case "dog": {
      return require("./dog.png");
    }
    case "fish": {
      return require("./fish.png");
    }
    case "hamster": {
      return require("./hamster.png");
    }
    default: {
      return require("./../icon.png");
    }
  }
};

export default getAnimals;
