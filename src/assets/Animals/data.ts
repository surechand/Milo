import { Pictogram, PictogramCategory } from "../../types/PictogramType";

const getAnimalsData: Pictogram[] = [
  {
    id: "Ani1",
    word: "kot",
    category: PictogramCategory.Animals,
    filename: "cat",
    custom: false,
  },
  {
    id: "Ani2",
    word: "pies",
    category: PictogramCategory.Animals,
    filename: "dog",
    custom: false,
  },
  {
    id: "Ani3",
    word: "ryba",
    category: PictogramCategory.Animals,
    filename: "fish",
    custom: false,
  },
  {
    id: "Ani4",
    word: "chomik",
    category: PictogramCategory.Animals,
    filename: "hamster",
    custom: false,
  },
];

export default getAnimalsData;
