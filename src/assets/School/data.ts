import { Pictogram, PictogramCategory } from "../../types/PictogramType";

const getSchoolData: Pictogram[] = [
  {
    id: "Sch1",
    word: "książka",
    category: PictogramCategory.School,
    filename: "book",
    custom: false,
  },
  {
    id: "Sch2",
    word: "lekcja",
    category: PictogramCategory.School,
    filename: "lesson",
    custom: false,
  },
  {
    id: "Sch3",
    word: "zeszyt",
    category: PictogramCategory.School,
    filename: "notebook",
    custom: false,
  },
];

export default getSchoolData;
