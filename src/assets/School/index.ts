const getSchool = (filename: string) => {
  switch (filename) {
    case "book": {
      return require("./book.png");
    }
    case "lesson": {
      return require("./lesson.png");
    }
    case "notebook": {
      return require("./notebook.png");
    }
    default: {
      return require("./../icon.png");
    }
  }
};

export default getSchool;
