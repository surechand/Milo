const getClothes = (filename: string) => {
  switch (filename) {
    case "shoes": {
      return require("./shoes.png");
    }
    case "trousers": {
      return require("./trousers.png");
    }
    case "tshirt": {
      return require("./tshirt.png");
    }
    default: {
      return require("./../icon.png");
    }
  }
};

export default getClothes;
