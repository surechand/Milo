import { Pictogram, PictogramCategory } from "../../types/PictogramType";

const getClothesData: Pictogram[] = [
  {
    id: "Clo1",
    word: "buty",
    category: PictogramCategory.Clothes,
    filename: "shoes",
    custom: false,
  },
  {
    id: "Clo2",
    word: "spodnie",
    category: PictogramCategory.Clothes,
    filename: "trousers",
    custom: false,
  },
  {
    id: "Clo3",
    word: "koszulka",
    category: PictogramCategory.Clothes,
    filename: "tshirt",
    custom: false,
  },
];

export default getClothesData;
