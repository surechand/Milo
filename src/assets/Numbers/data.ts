import { Pictogram, PictogramCategory } from "../../types/PictogramType";

const getNumbersData: Pictogram[] = [
  {
    id: "Num1",
    word: "1",
    category: PictogramCategory.Numbers,
    filename: "one",
    custom: false,
  },
  {
    id: "Num2",
    word: "2",
    category: PictogramCategory.Numbers,
    filename: "two",
    custom: false,
  },
  {
    id: "Num3",
    word: "3",
    category: PictogramCategory.Numbers,
    filename: "three",
    custom: false,
  },
  {
    id: "Num4",
    word: "4",
    category: PictogramCategory.Numbers,
    filename: "four",
    custom: false,
  },
  {
    id: "Num5",
    word: "5",
    category: PictogramCategory.Numbers,
    filename: "five",
    custom: false,
  },
  {
    id: "Num6",
    word: "6",
    category: PictogramCategory.Numbers,
    filename: "six",
    custom: false,
  },
  {
    id: "Num7",
    word: "7",
    category: PictogramCategory.Numbers,
    filename: "seven",
    custom: false,
  },
  {
    id: "Num8",
    word: "8",
    category: PictogramCategory.Numbers,
    filename: "eight",
    custom: false,
  },
  {
    id: "Num9",
    word: "9",
    category: PictogramCategory.Numbers,
    filename: "nine",
    custom: false,
  },
];

export default getNumbersData;
