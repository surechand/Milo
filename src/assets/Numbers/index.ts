const getNumbers = (filename: string) => {
  switch (filename) {
    case "one": {
      return require("./one.png");
    }
    case "two": {
      return require("./two.png");
    }
    case "three": {
      return require("./three.png");
    }
    case "four": {
      return require("./four.png");
    }
    case "five": {
      return require("./five.png");
    }
    case "six": {
      return require("./six.png");
    }
    case "seven": {
      return require("./seven.png");
    }
    case "eight": {
      return require("./eight.png");
    }
    case "nine": {
      return require("./nine.png");
    }
    default: {
      return require("./../icon.png");
    }
  }
};

export default getNumbers;
