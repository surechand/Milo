const getConjunctions = (filename: string) => {
  switch (filename) {
    case "and": {
      return require("./and.png");
    }
    case "if": {
      return require("./if.png");
    }
    case "or": {
      return require("./or.png");
    }
    default: {
      return require("./../icon.png");
    }
  }
};

export default getConjunctions;
