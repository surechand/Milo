import { Pictogram, PictogramCategory } from "../../types/PictogramType";

const getConjunctionsData: Pictogram[] = [
  {
    id: "Con1",
    word: "i",
    category: PictogramCategory.Conjunctions,
    filename: "and",
    custom: false,
  },
  {
    id: "Con2",
    word: "czy",
    category: PictogramCategory.Conjunctions,
    filename: "if",
    custom: false,
  },
  {
    id: "Con3",
    word: "lub",
    category: PictogramCategory.Conjunctions,
    filename: "or",
    custom: false,
  },
];

export default getConjunctionsData;
