import { Pictogram, PictogramCategory } from "../../types/PictogramType";

const getTimeData: Pictogram[] = [
  {
    id: "Tim1",
    word: "dzisiaj",
    category: PictogramCategory.Time,
    filename: "today",
    custom: false,
  },
  {
    id: "Tim2",
    word: "jutro",
    category: PictogramCategory.Time,
    filename: "tomorrow",
    custom: false,
  },
  {
    id: "Tim3",
    word: "wczoraj",
    category: PictogramCategory.Time,
    filename: "yesterday",
    custom: false,
  },
];

export default getTimeData;
