const getTime = (filename: string) => {
  switch (filename) {
    case "today": {
      return require("./today.png");
    }
    case "tomorrow": {
      return require("./tomorrow.png");
    }
    case "yesterday": {
      return require("./yesterday.png");
    }
    default: {
      return require("./../icon.png");
    }
  }
};

export default getTime;
