import { Pictogram, PictogramCategory } from "../../types/PictogramType";

const getActionsData: Pictogram[] = [
  {
    id: "Act1",
    word: "iść",
    category: PictogramCategory.Actions,
    filename: "walk",
    custom: false,
  },
  {
    id: "Act2",
    word: "chcieć",
    category: PictogramCategory.Actions,
    filename: "want",
    custom: false,
  },
  {
    id: "Act3",
    word: "robić",
    category: PictogramCategory.Actions,
    filename: "work",
    custom: false,
  },
];

export default getActionsData;
