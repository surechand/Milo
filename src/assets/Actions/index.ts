const getActions = (filename: string) => {
  switch (filename) {
    case "walk": {
      return require("./walk.png");
    }
    case "want": {
      return require("./want.png");
    }
    case "work": {
      return require("./work.png");
    }
    default: {
      return require("./../icon.png");
    }
  }
};

export default getActions;
