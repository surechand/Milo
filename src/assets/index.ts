import { Pictogram, PictogramCategory } from "../types/PictogramType";
import getActions from "./Actions/index";
import getAdjectives from "./Adjectives/index";
import getAnimals from "./Animals/index";
import getBody from "./Body/index";
import getClothes from "./Clothes/index";
import getConjunctions from "./Conjunctions/index";
import getFeelings from "./Feelings/index";
import getFood from "./Food/index";
import getHome from "./Home/index";
import getNumbers from "./Numbers/index";
import getPeople from "./People/index";
import getPlaces from "./Places/index";
import getSchool from "./School/index";
import getTime from "./Time/index";
import getTransport from "./Transport/index";
import getActionsData from "./Actions/data";
import getAdjectivesData from "./Adjectives/data";
import getAnimalsData from "./Animals/data";
import getBodyData from "./Body/data";
import getClothesData from "./Clothes/data";
import getConjunctionsData from "./Conjunctions/data";
import getFeelingsData from "./Feelings/data";
import getFoodData from "./Food/data";
import getHomeData from "./Home/data";
import getNumbersData from "./Numbers/data";
import getPeopleData from "./People/data";
import getPlacesData from "./Places/data";
import getSchoolData from "./School/data";
import getTimeData from "./Time/data";
import getTransportData from "./Transport/data";
import { ImageSourcePropType, ImageURISource } from "react-native";

export const getCategoryIconsSource = (
  category: PictogramCategory
): ImageURISource => {
  switch (category) {
    case PictogramCategory.Actions: {
      return require("./Actions/categoryIcon.png");
    }
    case PictogramCategory.Adjectives: {
      return require("./Adjectives/categoryIcon.png");
    }
    case PictogramCategory.Animals: {
      return require("./Animals/categoryIcon.png");
    }
    case PictogramCategory.Body: {
      return require("./Body/categoryIcon.png");
    }
    case PictogramCategory.Clothes: {
      return require("./Clothes/categoryIcon.png");
    }
    case PictogramCategory.Conjunctions: {
      return require("./Conjunctions/categoryIcon.png");
    }
    case PictogramCategory.Feelings: {
      return require("./Feelings/categoryIcon.png");
    }
    case PictogramCategory.Food: {
      return require("./Food/categoryIcon.png");
    }
    case PictogramCategory.Home: {
      return require("./Home/categoryIcon.png");
    }
    case PictogramCategory.Numbers: {
      return require("./Numbers/categoryIcon.png");
    }
    case PictogramCategory.People: {
      return require("./People/categoryIcon.png");
    }
    case PictogramCategory.Places: {
      return require("./Places/categoryIcon.png");
    }
    case PictogramCategory.School: {
      return require("./School/categoryIcon.png");
    }
    case PictogramCategory.Time: {
      return require("./Time/categoryIcon.png");
    }
    case PictogramCategory.Transport: {
      return require("./Transport/categoryIcon.png");
    }
    default: {
      return require("./icon.png");
    }
  }
};

export const getIconsData = (category: PictogramCategory): Pictogram[] => {
  switch (category) {
    case PictogramCategory.Actions: {
      return getActionsData;
    }
    case PictogramCategory.Adjectives: {
      return getAdjectivesData;
    }
    case PictogramCategory.Animals: {
      return getAnimalsData;
    }
    case PictogramCategory.Body: {
      return getBodyData;
    }
    case PictogramCategory.Clothes: {
      return getClothesData;
    }
    case PictogramCategory.Conjunctions: {
      return getConjunctionsData;
    }
    case PictogramCategory.Feelings: {
      return getFeelingsData;
    }
    case PictogramCategory.Food: {
      return getFoodData;
    }
    case PictogramCategory.Home: {
      return getHomeData;
    }
    case PictogramCategory.Numbers: {
      return getNumbersData;
    }
    case PictogramCategory.People: {
      return getPeopleData;
    }
    case PictogramCategory.Places: {
      return getPlacesData;
    }
    case PictogramCategory.School: {
      return getSchoolData;
    }
    case PictogramCategory.Time: {
      return getTimeData;
    }
    case PictogramCategory.Transport: {
      return getTransportData;
    }
    default: {
      return require("./icon.png");
    }
  }
};

export const getWordIconsSource = (
  category: PictogramCategory,
  filename: string
): ImageSourcePropType => {
  switch (category) {
    case PictogramCategory.Actions: {
      return getActions(filename);
    }
    case PictogramCategory.Adjectives: {
      return getAdjectives(filename);
    }
    case PictogramCategory.Animals: {
      return getAnimals(filename);
    }
    case PictogramCategory.Body: {
      return getBody(filename);
    }
    case PictogramCategory.Clothes: {
      return getClothes(filename);
    }
    case PictogramCategory.Conjunctions: {
      return getConjunctions(filename);
    }
    case PictogramCategory.Feelings: {
      return getFeelings(filename);
    }
    case PictogramCategory.Food: {
      return getFood(filename);
    }
    case PictogramCategory.Home: {
      return getHome(filename);
    }
    case PictogramCategory.Numbers: {
      return getNumbers(filename);
    }
    case PictogramCategory.People: {
      return getPeople(filename);
    }
    case PictogramCategory.Places: {
      return getPlaces(filename);
    }
    case PictogramCategory.School: {
      return getSchool(filename);
    }
    case PictogramCategory.Time: {
      return getTime(filename);
    }
    case PictogramCategory.Transport: {
      return getTransport(filename);
    }
    default: {
      return require("./icon.png");
    }
  }
};
