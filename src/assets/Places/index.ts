const getPlaces = (filename: string) => {
  switch (filename) {
    case "home": {
      return require("./home.png");
    }
    case "restaurant": {
      return require("./restaurant.png");
    }
    case "school": {
      return require("./school.png");
    }
    default: {
      return require("./../icon.png");
    }
  }
};

export default getPlaces;
