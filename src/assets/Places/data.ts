import { Pictogram, PictogramCategory } from "../../types/PictogramType";

const getPlacesData: Pictogram[] = [
  {
    id: "Pla1",
    word: "dom",
    category: PictogramCategory.Places,
    filename: "home",
    custom: false,
  },
  {
    id: "Pla2",
    word: "restauracja",
    category: PictogramCategory.Places,
    filename: "restaurant",
    custom: false,
  },
  {
    id: "Pla3",
    word: "szkoła",
    category: PictogramCategory.Places,
    filename: "school",
    custom: false,
  },
];

export default getPlacesData;
