import React, { useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import AboutView from "../modules/sideMenuModule/AboutView";
import { DrawerNavParamList } from "./NavigationParamLists";
import MainViewNavigator from "./MainViewNavigator";
import { ListContext } from "../context/ListContext";
import { SentenceContext } from "../context/SentenceContext";
import { Sentence } from "../types/SentenceType";
import { Pictogram } from "../types/PictogramType";

const Drawer = createDrawerNavigator<DrawerNavParamList>();

const CombinedStackNavigator = () => {
  const [sentence, setSentence] = useState<Pictogram[]>([]);
  const [favouritesList, setFavouritesList] = useState<Sentence[]>([]);

  return (
    <NavigationContainer>
      <ListContext.Provider value={{ favouritesList, setFavouritesList }}>
        <SentenceContext.Provider value={{ sentence, setSentence }}>
          <Drawer.Navigator initialRouteName="MainViewNavigator">
            <Drawer.Screen
              name="MainViewNavigator"
              component={MainViewNavigator}
              options={{ headerShown: false, title: "Start" }}
            />
            <Drawer.Screen
              name="AboutView"
              component={AboutView}
              options={{ headerShown: false, title: "O projekcie" }}
            />
          </Drawer.Navigator>
        </SentenceContext.Provider>
      </ListContext.Provider>
    </NavigationContainer>
  );
};

export default CombinedStackNavigator;
