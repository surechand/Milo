import { StackScreenProps } from "@react-navigation/stack";
import { DrawerScreenProps } from "@react-navigation/drawer";
import { CompositeScreenProps } from "@react-navigation/native";

export type DrawerNavParamList = {
  MainViewNavigator: undefined;
  AboutView: undefined;
};

export type MainStackParamList = {
  MainView: undefined;
  LibraryView: undefined;
  FavouriteView: undefined;
};

export type CombinedStacksProps = CompositeScreenProps<
  StackScreenProps<MainStackParamList, "MainView">,
  DrawerScreenProps<DrawerNavParamList, "MainViewNavigator">
>;

export type FavouriteViewStackProps = StackScreenProps<
  MainStackParamList,
  "FavouriteView"
>;
