import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import MainView from "../modules/mainViewModule/MainView";
import LibraryView from "../modules/libraryModule/LibraryView";
import FavouriteView from "../modules/favouritesModule/FavouriteView";
import { MainStackParamList } from "./NavigationParamLists";

const Stack = createStackNavigator<MainStackParamList>();

const MainViewNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="MainView"
        component={MainView}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="LibraryView"
        component={LibraryView}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="FavouriteView"
        component={FavouriteView}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

export default MainViewNavigator;
