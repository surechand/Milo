import { Dimensions, StyleSheet } from "react-native";
import { ColourPalette } from "../../styles/ColourPalette";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: ColourPalette.FloralWhite,
    flexDirection: "column",
  },
  tableContainer: {
    flex: 1,
    flexGrow: 5,
    backgroundColor: ColourPalette.FloralWhite,
  },
  row: {
    flex: 1,
    padding: 6,
    backgroundColor: ColourPalette.FloralWhite,
    flexDirection: "row",
    borderWidth: 1,
    borderColor: ColourPalette.Jet,
  },
  text: {
    flex: 9,
    fontSize: 36,
    color: ColourPalette.Jet,
  },
  deleteButton: {
    flex: 1,
    color: ColourPalette.CandyPink,
  },
  deleteButtonText: {
    fontSize: Dimensions.get("window").height / 10,
    textAlign: "center",
    color: ColourPalette.FloralWhite,
  },
  deleteIcon: {
    color: ColourPalette.CandyPink,
    padding: 6,
    alignItems: "center",
    justifyContent: "center",
  },
  uploadIcon: {
    color: ColourPalette.Jet,
    padding: 6,
    alignItems: "center",
    justifyContent: "center",
  },
  bottomRow: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: ColourPalette.FloralWhite,
    padding: 6,
  },
  returnButton: {
    flex: 2,
    backgroundColor: ColourPalette.Timberwolf,
    alignItems: "center",
    justifyContent: "center",
    padding: 6,
    borderRadius: 10,
    margin: 6,
  },
  returnIcon: {
    color: ColourPalette.Jet,
    fontSize: Dimensions.get("window").height,
  },
  clearButton: {
    flex: 3,
    backgroundColor: ColourPalette.CandyPink,
    alignItems: "center",
    justifyContent: "center",
    padding: 6,
    borderRadius: 10,
    margin: 6,
  },
});

export default styles;
