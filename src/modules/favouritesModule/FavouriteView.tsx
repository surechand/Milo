import React, { useContext, useEffect, useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import styles from "./FavouriteView.Styles";
import { ScrollView } from "react-native-gesture-handler";
import { AntDesign, Ionicons, Feather } from "@expo/vector-icons";
import { Pictogram } from "../../types/PictogramType";
import { FavouriteViewStackProps } from "../../navigation/NavigationParamLists";
import { LogBox } from "react-native";
import FavouritesListController from "./FavouritesListController";
import { Sentence } from "../../types/SentenceType";
import { ListContext } from "../../context/ListContext";
import SentenceController from "../mainViewModule/sentencePreview/SentenceController";

LogBox.ignoreLogs([
  "Non-serializable values were found in the navigation state",
]);

const FavouriteView = ({ navigation }: FavouriteViewStackProps) => {
  const { favouritesList } = useContext(ListContext);
  const { deleteFromFavouritesByIndex, clearFavourites } =
    FavouritesListController();
  const { setNewSentence } = SentenceController();
  const [localList, setLocalList] = useState<Sentence[]>([]);

  const getIDs = (sentence: Pictogram[]) => {
    const buffer: string[] = [];
    sentence.forEach((el) => {
      buffer.push(el.id);
    });
    return buffer.toLocaleString();
  };

  const getWords = (sentence: Pictogram[]) => {
    const buffer: string[] = [];
    sentence.forEach((el) => {
      buffer.push(el.word);
    });
    return buffer.toLocaleString();
  };

  const setCategoryData = async () => {
    if (favouritesList != undefined) {
      setLocalList(favouritesList);
    }
  };

  const loadSentence = (sentence: Pictogram[]) => {
    setNewSentence(sentence);
  };

  useEffect(() => {
    setCategoryData();
  }, [favouritesList]);

  return (
    <View style={styles.container}>
      <ScrollView style={styles.tableContainer}>
        {localList.map((el, index) => (
          <View style={styles.row} key={getIDs(el.sentence)}>
            <Text style={styles.text}>{getWords(el.sentence)}</Text>
            <TouchableOpacity
              key={index.toString().concat("uploadbutton")}
              style={styles.deleteButton}
              onPress={() => {
                loadSentence(el.sentence);
                navigation.navigate("MainView");
              }}
            >
              <Feather
                key={index.toString().concat("uploadicon")}
                name="upload"
                size={styles.text.fontSize}
                style={styles.uploadIcon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              key={index.toString().concat("deletebutton")}
              style={styles.deleteButton}
              onPress={async () => {
                await deleteFromFavouritesByIndex(el.id);
              }}
            >
              <AntDesign
                key={index.toString().concat("deleteicon")}
                name="delete"
                size={styles.text.fontSize}
                style={styles.deleteIcon}
              />
            </TouchableOpacity>
          </View>
        ))}
      </ScrollView>
      <View style={styles.bottomRow}>
        <TouchableOpacity
          style={styles.returnButton}
          onPress={() => {
            navigation.navigate("MainView");
          }}
        >
          <Ionicons
            name="return-up-back"
            style={styles.returnIcon}
            adjustsFontSizeToFit
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.clearButton}
          onPress={async () => {
            await clearFavourites();
          }}
        >
          <Text style={styles.deleteButtonText} adjustsFontSizeToFit>
            Wyczyść listę
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default FavouriteView;
