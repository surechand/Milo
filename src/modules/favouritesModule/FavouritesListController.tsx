import { useContext, useEffect } from "react";
import {
  getDataAsync,
  getItemIndexAsync,
  storeDataAsync,
  removeSentenceAsync,
  clearKey,
} from "../../features/storage/AsyncStorageHandler";
import { Pictogram } from "../../types/PictogramType";
import { Sentence } from "../../types/SentenceType";
import { ListContext } from "../../context/ListContext";

const FavouritesListController = (): {
  addToFavourites: (newSentence: Pictogram[]) => Promise<void>;
  deleteFromFavouritesByIndex: (sentenceIndex: number) => Promise<void>;
  deleteFromFavouritesBySentence: (sentence: Pictogram[]) => Promise<void>;
  checkIfInFavourites: (sentence: Pictogram[]) => boolean;
  clearFavourites: () => Promise<void>;
} => {
  const { favouritesList, setFavouritesList } = useContext(ListContext);
  const storageKey = "favs";

  const setFavouritesData = async () => {
    const getSentences = getDataAsync<Sentence[]>(storageKey);
    const sentenceArray = await getSentences();
    if (sentenceArray != null) {
      setFavouritesList(sentenceArray);
    } else {
      setFavouritesList([]);
    }
  };

  useEffect(() => {
    setFavouritesData();
  }, []);

  const addToFavourites = async (newSentence: Pictogram[]) => {
    const newIndex = await getItemIndexAsync(storageKey);
    const newElement: Sentence = {
      id: newIndex,
      sentence: newSentence,
    };
    const storeData = storeDataAsync<Sentence>(storageKey);
    await storeData(newElement);
    setFavouritesData();
  };

  const deleteFromFavouritesByIndex = async (sentenceIndex: number) => {
    await removeSentenceAsync(storageKey, sentenceIndex);
    setFavouritesData();
  };

  const deleteFromFavouritesBySentence = async (sentence: Pictogram[]) => {
    if (favouritesList != undefined) {
      if (favouritesList.length > 0 && favouritesList != null) {
        let tempCheck = false;
        let sentenceIndex: number | undefined;
        favouritesList.forEach((el) => {
          tempCheck = false;
          if (el.sentence.length == sentence.length) {
            for (let i = 0; i < el.sentence.length; i++) {
              if (el.sentence[i].id == sentence[i].id) {
                tempCheck = true;
              } else {
                tempCheck = false;
              }
            }
            if (tempCheck == true) {
              sentenceIndex = el.id;
            }
          }
        });
        if (typeof sentenceIndex !== "undefined") {
          await removeSentenceAsync(storageKey, sentenceIndex);
          setFavouritesData();
        }
      }
    }
  };

  const checkIfInFavourites = (sentence: Pictogram[]) => {
    let isInFavourites = false;
    if (favouritesList != undefined) {
      if (favouritesList.length > 0 && favouritesList != null) {
        let tempCheck = false;
        favouritesList.forEach((el) => {
          tempCheck = false;
          if (el.sentence.length == sentence.length) {
            for (let i = 0; i < el.sentence.length; i++) {
              if (el.sentence[i].id == sentence[i].id) {
                tempCheck = true;
              } else {
                tempCheck = false;
              }
            }
            if (tempCheck == true) {
              isInFavourites = true;
            }
          }
        });
      }
    }
    return isInFavourites;
  };

  const clearFavourites = async () => {
    await clearKey(storageKey);
    setFavouritesData();
  };

  return {
    addToFavourites,
    deleteFromFavouritesByIndex,
    deleteFromFavouritesBySentence,
    checkIfInFavourites,
    clearFavourites,
  };
};

export default FavouritesListController;
