import React from "react";
import { View, Text, Button } from "react-native";
import * as WebBrowser from "expo-web-browser";
import { clearAllStorage } from "../../features/storage/AsyncStorageHandler";

const AboutView = (): JSX.Element => {
  const openIconCreator = () => {
    WebBrowser.openBrowserAsync("https://www.freepik.com/");
  };
  const openIconStore = () => {
    WebBrowser.openBrowserAsync("https://www.flaticon.com/");
  };
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <View style={{ flex: 3, alignItems: "center", justifyContent: "center" }}>
        <Text style={{ fontSize: 20, fontWeight: "700" }}>
          Projekt inżynierski stworzony przez Krzysztofa Deca, studenta
          Informatyki na Politechnice Śląskiej w Gliwicach.{"\n\n"}
          Promotor: dr.inż. Krzysztof Dobosz.{"\n\n"}
        </Text>
        <Text
          style={{
            color: "blue",
            fontSize: 40,
            fontWeight: "700",
            textDecorationLine: "underline",
          }}
          onPress={() => openIconCreator()}
        >
          Icons made by Freepik
        </Text>
        <Text
          style={{
            color: "blue",
            fontSize: 40,
            fontWeight: "700",
            textDecorationLine: "underline",
          }}
          onPress={() => openIconStore()}
        >
          from www.flaticon.com
        </Text>
      </View>
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Button
          onPress={clearAllStorage}
          title={"Przywróc ustawienia domyślne aplikacji"}
        />
      </View>
    </View>
  );
};

export default AboutView;
