import React, { useContext } from "react";
import * as Config from "../../../config/Config";
import { TouchableOpacity, Text, Image, View } from "react-native";
import styles from "./SentenceHeaderBar.styles";
import { getWordIconsSource } from "../../../assets/index";
import { SentenceContext } from "../../../context/SentenceContext";
import SentenceController from "./SentenceController";

const SentenceHeaderBar = (): {
  WordPreviewRow: () => JSX.Element;
} => {
  const { deleteWord } = SentenceController();
  const { sentence } = useContext(SentenceContext);

  const WordPreviewRow = () => {
    const RowBuffer = [];
    sentence.forEach((el, index) => {
      RowBuffer.push(
        <TouchableOpacity
          key={el.id.concat(index.toString())}
          style={styles.pictogramStyle}
          onLongPress={() => deleteWord(sentence.indexOf(el))}
        >
          <Image
            key={el.id.concat("icon")}
            source={
              el.custom
                ? { uri: el.filename }
                : getWordIconsSource(el.category, el.filename)
            }
            style={styles.pictogramThumbnail}
          />
          <View style={styles.textContainer}>
            <Text
              style={styles.buttonText}
              adjustsFontSizeToFit
              numberOfLines={1}
            >
              {el.word}
            </Text>
          </View>
        </TouchableOpacity>
      );
    });
    if (sentence.length < Config.SentenceMaxLenght) {
      for (let i = 0; i < Config.SentenceMaxLenght - sentence.length; i++) {
        RowBuffer.push(
          <TouchableOpacity key={i} style={styles.pictogramStyle}>
            <Text style={styles.buttonText}></Text>
          </TouchableOpacity>
        );
      }
    }
    return <>{RowBuffer}</>;
  };

  return {
    WordPreviewRow,
  };
};

export default SentenceHeaderBar;
