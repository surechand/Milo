import { Dimensions, StyleSheet } from "react-native";
import { ColourPalette } from "../../../styles/ColourPalette";

const styles = StyleSheet.create({
  pictogramStyle: {
    flex: 1,
    padding: 10,
    margin: 6,
    borderRadius: 10,
    height: "75%",
    backgroundColor: ColourPalette.Cream,
    justifyContent: "center",
    alignItems: "center",
  },
  textContainer: {
    height: "20%",
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    fontSize: Dimensions.get("window").height,
    fontWeight: "bold",
    textAlign: "center",
    color: ColourPalette.Jet,
  },
  pictogramThumbnail: {
    flex: 4,
    marginTop: 20,
    width: "50%",
    height: "50%",
    resizeMode: "contain",
  },
});

export default styles;
