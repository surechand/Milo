import { useContext } from "react";
import { Pictogram } from "../../../types/PictogramType";
import * as Config from "../../../config/Config";
import { SentenceContext } from "../../../context/SentenceContext";

const SentenceController = (): {
  addWord: (newWord: Pictogram) => void;
  deleteWord: (wordIndex: number) => void;
  setNewSentence: (newSentence: Pictogram[]) => void;
  clearSentence: () => void;
} => {
  const { sentence, setSentence } = useContext(SentenceContext);

  const addWord = (newWord: Pictogram) => {
    if (sentence.length < Config.SentenceMaxLenght) {
      setSentence([...sentence, newWord]);
    }
  };

  const deleteWord = (wordIndex: number) => {
    setSentence(sentence.filter((item, index) => index !== wordIndex));
  };

  const setNewSentence = (newSentence: Pictogram[]) => {
    setSentence(newSentence);
  };

  const clearSentence = () => {
    setSentence([]);
  };

  return {
    addWord,
    deleteWord,
    setNewSentence,
    clearSentence,
  };
};

export default SentenceController;
