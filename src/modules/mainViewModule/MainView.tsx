import React, { useContext, useEffect, useState } from "react";
import { View, TouchableOpacity, Text, Image } from "react-native";
import styles from "./MainView.Styles";
import { speakWord, speakSentence } from "../../features/speech/Speech";
import { shortcuts } from "../../config/Config";
import { CombinedStacksProps } from "../../navigation/NavigationParamLists";
import SentenceController from "./sentencePreview/SentenceController";
import { Pictogram } from "../../types/PictogramType";
import FavouritesListController from "../favouritesModule/FavouritesListController";
import { FontAwesome, SimpleLineIcons } from "@expo/vector-icons";
import { getWordIconsSource } from "../../assets";
import { ListContext } from "../../context/ListContext";
import { SentenceContext } from "../../context/SentenceContext";
import SentenceHeaderBar from "./sentencePreview/SentenceHeaderBar";

const MainView = ({ navigation }: CombinedStacksProps): JSX.Element => {
  const { addWord, clearSentence } = SentenceController();
  const { WordPreviewRow } = SentenceHeaderBar();
  const { sentence } = useContext(SentenceContext);
  const { favouritesList } = useContext(ListContext);
  const {
    addToFavourites,
    deleteFromFavouritesBySentence,
    checkIfInFavourites,
  } = FavouritesListController();
  const [inFavourites, setInFavourites] = useState<boolean>(false);

  const PictogramButton = (pictogram: Pictogram) => (
    <TouchableOpacity
      key={pictogram.id}
      style={styles.pictogramStyle}
      onPress={() => {
        speakWord(pictogram.word);
        addWord(pictogram);
      }}
    >
      <Image
        key={pictogram.filename}
        resizeMethod="scale"
        resizeMode="cover"
        source={
          pictogram.custom
            ? { uri: pictogram.filename }
            : getWordIconsSource(pictogram.category, pictogram.filename)
        }
        style={styles.thumbnail}
      />
      <Text style={styles.text}>{pictogram.word}</Text>
    </TouchableOpacity>
  );

  //Check if favourite when adding new word
  useEffect(() => {
    setInFavourites(checkIfInFavourites(sentence));
  }, [sentence]);

  //Check if favourite when deleting sentence in favourites view
  useEffect(() => {
    setInFavourites(checkIfInFavourites(sentence));
  }, [favouritesList]);

  return (
    <View style={styles.mainView}>
      <View style={styles.pictogramPanel}>
        <View style={styles.sentencePreview}>
          <WordPreviewRow />
        </View>
        <View style={styles.pictogramPicker}>
          <PictogramButton {...shortcuts[0]} />
          <PictogramButton {...shortcuts[1]} />
          <PictogramButton {...shortcuts[2]} />
          <TouchableOpacity
            style={styles.clearButtonStyle}
            onPress={() => clearSentence()}
          >
            <Text style={styles.clearButtonText} adjustsFontSizeToFit>
              {"Wyczyść"}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.navigationPanel}>
        <TouchableOpacity
          style={styles.drawerButton}
          onPress={() => navigation.toggleDrawer()}
        >
          <SimpleLineIcons
            name="options-vertical"
            style={styles.drawerIcon}
            adjustsFontSizeToFit
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.libraryButton}
          onPress={() => navigation.navigate("LibraryView")}
        >
          <Text style={styles.navButtonText} adjustsFontSizeToFit>
            Biblioteka
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonStyle}
          onPress={async () => {
            await speakSentence(sentence);
            clearSentence();
          }}
        >
          <Text style={styles.navButtonText} adjustsFontSizeToFit>
            Start
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.favButton}
          onPress={() => navigation.navigate("FavouriteView")}
        >
          <Text style={styles.navButtonText} adjustsFontSizeToFit>
            Ulubione
          </Text>
        </TouchableOpacity>
        {inFavourites ? (
          <TouchableOpacity
            style={styles.starButton}
            onPress={async () => {
              if (checkIfInFavourites(sentence)) {
                await deleteFromFavouritesBySentence(sentence);
                setInFavourites(false);
              }
            }}
          >
            <FontAwesome
              name="star"
              style={styles.starIcon}
              adjustsFontSizeToFit
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={styles.starButton}
            onPress={async () => {
              if (sentence.length > 0) {
                if (!checkIfInFavourites(sentence)) {
                  await addToFavourites(sentence);
                  setInFavourites(true);
                }
              }
            }}
          >
            <FontAwesome
              name="star-o"
              style={styles.starIcon}
              adjustsFontSizeToFit
            />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default MainView;
