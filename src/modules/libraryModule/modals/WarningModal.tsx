import React, { Dispatch, SetStateAction, useEffect, useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  Modal,
  StyleSheet,
} from "react-native";
import { categoryTab } from "../styles/PictogramDirectory.Styles";
import { Pictogram, PictogramCategory } from "../../../types/PictogramType";
import { speakWord } from "../../../features/speech/Speech";
import { getIconsData, getWordIconsSource } from "../../../assets/index";
import {
  getDataAsync,
  removePictogramAsync,
} from "../../../features/storage/AsyncStorageHandler";
import { useCallback } from "react";

interface ModalProps {
  pictogramId: string;
}

const WarningModal = ({ pictogramId }: ModalProps): JSX.Element => {
  return <View style={categoryTab.container}></View>;
};
