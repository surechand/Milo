import { Dimensions, StyleSheet } from "react-native";
import { ColourPalette } from "../../../styles/ColourPalette";

export const categoryTab = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: ColourPalette.FloralWhite,
    alignItems: "center",
    justifyContent: "center",
  },
  contentContainer: {
    flexGrow: 1,
  },
  headline: {
    height: Dimensions.get("window").height / 7,
    width: Dimensions.get("window").width,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: ColourPalette.Jet,
  },
  headlineText: {
    fontSize: 50,
    color: ColourPalette.FloralWhite,
  },
  tabContainer: {
    flex: 4,
  },
  text: {
    fontSize: 36,
    color: ColourPalette.Jet,
  },
  returnButton: {
    flex: 1,
    height: Dimensions.get("window").height / 9,
    width: Dimensions.get("window").width / 4,
    position: "absolute",
    left: Dimensions.get("window").width / 20,
    backgroundColor: ColourPalette.Cream,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  returnButtonText: {
    fontSize: 36,
    textAlign: "center",
    color: ColourPalette.Jet,
  },
  bottomRow: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: ColourPalette.FloralWhite,
    padding: 6,
  },
  pictogramStyle: {
    flexBasis: "30%",
    padding: 10,
    margin: 10,
    marginTop: 20,
    marginBottom: 20,
    borderRadius: 10,
    backgroundColor: ColourPalette.Cream,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
  thumbnail: {
    marginTop: 20,
    width: "85%",
    height: "85%",
    resizeMode: "contain",
  },
  flatListRow: {
    height: Dimensions.get("window").height / 2.5,
    width: Dimensions.get("window").width,
    flexDirection: "row",
    backgroundColor: ColourPalette.FloralWhite,
    padding: 6,
    justifyContent: "space-evenly",
  },
});
