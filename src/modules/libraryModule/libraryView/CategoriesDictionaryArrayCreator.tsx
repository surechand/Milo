import { PictogramCategory } from "../../../types/PictogramType";

const getCategoriesArray = () => {
  const labelsArray = Object.values(PictogramCategory);
  const keysArray = Object.keys(PictogramCategory);
  const zippedArray = labelsArray.map((label, index) => ({
    label: label,
    value: keysArray[index],
  }));
  return zippedArray.sort((el1, el2) => el1.label.localeCompare(el2.label));
};

export default getCategoriesArray;
