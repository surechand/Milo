import React from "react";
import { Text, TouchableOpacity, Image } from "react-native";
import { PictogramCategory } from "../../../types/PictogramType";
import { getCategoryIconsSource } from "../../../assets/index";
import { library } from "../styles/LibraryView.Styles";

interface ButtonProps {
  label: PictogramCategory;
  onPress: (label: PictogramCategory) => void;
}

export const CategoryButton = ({
  label,
  onPress,
}: ButtonProps): JSX.Element => (
  <TouchableOpacity
    key={label}
    style={library.pictogramStyle}
    onPress={() => onPress(label)}
  >
    <Image
      key={label}
      source={getCategoryIconsSource(label)}
      style={library.categoryThumbnail}
    />
    <Text style={library.text} adjustsFontSizeToFit>
      {label.toString()}
    </Text>
  </TouchableOpacity>
);
