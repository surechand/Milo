import React, { useCallback, useState } from "react";
import { View, FlatList, TouchableOpacity, Text } from "react-native";
import { library } from "../styles/LibraryView.Styles";
import { Pictogram, PictogramCategory } from "../../../types/PictogramType";
import CategoryPreview from "../categoryView/CategoryPreview";
import { CombinedStacksProps } from "../../../navigation/NavigationParamLists";
import { CategoryButton } from "./CategoryButton";
import SentenceController from "../../mainViewModule/sentencePreview/SentenceController";

const LibraryCategoriesView = ({
  navigation,
}: CombinedStacksProps): JSX.Element => {
  const pictogramCategories = Object.values(PictogramCategory).sort(
    (el1, el2) => el1.localeCompare(el2)
  );
  const [selectedCategory, setSelectedCategory] =
    useState<PictogramCategory | null>(null);
  const { addWord } = SentenceController();

  const createFlatListRows = () => {
    const rowBuffer = [];
    for (let i = 0; i < pictogramCategories.length; i += 3) {
      rowBuffer.push([
        pictogramCategories[i],
        pictogramCategories[i + 1],
        pictogramCategories[i + 2],
      ]);
    }
    return rowBuffer;
  };

  const flatListRowArray = createFlatListRows();

  const onCategoryButtonPress = (label: PictogramCategory) => {
    setSelectedCategory(label);
  };

  const onPictogramButtonPress = (pictogram: Pictogram) => {
    addWord(pictogram);
    navigation.navigate("MainView");
  };

  const FlatListRow = useCallback(
    (categoriesRow: {
      category1: PictogramCategory;
      category2: PictogramCategory;
      category3: PictogramCategory;
    }) => (
      <View style={library.flatListRow}>
        <CategoryButton
          label={categoriesRow.category1}
          onPress={onCategoryButtonPress}
        />
        <CategoryButton
          label={categoriesRow.category2}
          onPress={onCategoryButtonPress}
        />
        <CategoryButton
          label={categoriesRow.category3}
          onPress={onCategoryButtonPress}
        />
      </View>
    ),
    []
  );

  return (
    <View style={library.mainViewContainer}>
      {selectedCategory ? (
        <View style={library.container}>
          <View style={library.headline}>
            <TouchableOpacity
              style={library.returnToCategoriesButton}
              onPress={() => setSelectedCategory(null)}
            >
              <Text
                style={library.returnToCategoriesButtonText}
                adjustsFontSizeToFit
              >
                {"Powrót"}
              </Text>
            </TouchableOpacity>
            <Text style={library.headlineText} adjustsFontSizeToFit>
              {selectedCategory}
            </Text>
          </View>

          <CategoryPreview
            selectedCategory={selectedCategory}
            pictogramSelector={onPictogramButtonPress}
          />
        </View>
      ) : (
        <FlatList
          contentContainerStyle={library.contentContainer}
          data={flatListRowArray}
          keyExtractor={(categoriesRow) =>
            categoriesRow[0].concat(categoriesRow[1]).concat(categoriesRow[2])
          }
          renderItem={(categoriesRow) => (
            <FlatListRow
              category1={categoriesRow.item[0]}
              category2={categoriesRow.item[1]}
              category3={categoriesRow.item[2]}
            />
          )}
        />
      )}
    </View>
  );
};

export default LibraryCategoriesView;
