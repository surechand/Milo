import React, { Dispatch, SetStateAction, useEffect, useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
} from "react-native";
import { creator } from "./styles/PictogramCreator.Styles";
import { Pictogram, PictogramCategory } from "../../types/PictogramType";
import {
  storeDataAsync,
  getItemIndexAsync,
} from "../../features/storage/AsyncStorageHandler";
import * as ImagePicker from "expo-image-picker";
import DropDownPicker from "react-native-dropdown-picker";
import getCategoriesArray from "./libraryView/CategoriesDictionaryArrayCreator";

interface CreatorProps {
  creatorEnablerDispatch: Dispatch<SetStateAction<boolean>>;
}

const PictogramCreator = ({
  creatorEnablerDispatch,
}: CreatorProps): JSX.Element => {
  const [imageUploaded, setImageUploaded] = useState<boolean>(false);
  const [selectedImage, setSelectedImage] = useState<{ localUri: string }>();
  const [newPicogramName, setNewPictogramName] = useState<string>("");
  const [newPicogramCategory, setNewPictogramCategory] =
    useState<PictogramCategory | null>(null);
  const [open, setOpen] = useState<boolean>(false);
  const [items, setItems] = useState(getCategoriesArray());
  const [newPictogramProps, setNewPictogramProps] = useState<
    Pictogram | undefined
  >(undefined);

  const openImagePicker = async (useCamera: boolean) => {
    const permissionResult =
      await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (permissionResult.granted === false) {
      alert("Permission to access camera roll is required!");
      return;
    }
    const pickerConfig: ImagePicker.ImagePickerOptions = {
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [1, 1],
    };
    const pickerResult = useCamera
      ? await ImagePicker.launchCameraAsync(pickerConfig)
      : await ImagePicker.launchImageLibraryAsync(pickerConfig);
    if (pickerResult.canceled === true) {
      return;
    }
    setSelectedImage({ localUri: pickerResult.assets[0].uri });
    setImageUploaded(true);
  };

  const savePictogramHandler = async () => {
    if (
      newPicogramName.length > 1 &&
      newPicogramName != null &&
      newPicogramCategory != null &&
      selectedImage?.localUri != undefined
    ) {
      const newIndex = await getItemIndexAsync(
        newPicogramCategory.substring(0, 3)
      );
      setNewPictogramProps({
        id: "c_"
          .concat(newPicogramCategory.substring(0, 3))
          .concat(newIndex.toString()),
        word: newPicogramName.toLowerCase(),
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        category: newPicogramCategory!,
        filename: selectedImage.localUri,
        custom: true,
      });
    }
  };

  useEffect(() => {
    const storePictogramAsync = async () => {
      if (newPicogramCategory != null && newPictogramProps != undefined) {
        const storeData = storeDataAsync<Pictogram>(
          newPicogramCategory.substring(0, 3)
        );
        await storeData(newPictogramProps);
        creatorEnablerDispatch(false);
      }
    };
    storePictogramAsync();
  }, [newPictogramProps]);

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={creator.creatorView}>
        <View style={creator.imagePreview}>
          {imageUploaded ? (
            <Image
              key={selectedImage?.localUri}
              source={{ uri: selectedImage?.localUri }}
              style={creator.thumbnail}
            />
          ) : (
            <View style={creator.imagePreview}>
              <TouchableOpacity
                style={creator.uploadButton}
                onPress={() => openImagePicker(false)}
              >
                <Text style={creator.uploadButtonText}>
                  {"Wybierz zdjęcie \n z galerii"}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={creator.uploadButton}
                onPress={() => openImagePicker(true)}
              >
                <Text style={creator.uploadButtonText}>{"Zrób zdjęcie"}</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
        <View style={creator.paramsInput}>
          <TextInput
            style={creator.input}
            value={newPicogramName}
            onChangeText={setNewPictogramName}
            placeholder="Nazwa"
            multiline={false}
          />
          <DropDownPicker
            style={creator.dropdown}
            labelStyle={creator.labelContainer}
            dropDownContainerStyle={creator.dropdownContainer}
            listItemContainerStyle={creator.listItemContainer}
            textStyle={creator.listItemText}
            dropDownDirection="BOTTOM"
            open={open}
            value={newPicogramCategory}
            items={items}
            setOpen={setOpen}
            setValue={setNewPictogramCategory}
            setItems={setItems}
            itemSeparator={true}
            placeholder="Wybierz kategorię"
          />
          <TouchableOpacity
            style={creator.saveButton}
            onPress={() => {
              savePictogramHandler();
            }}
          >
            <Text style={creator.saveButtonText}>{"Zapisz"}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={creator.cancelButton}
            onPress={() => creatorEnablerDispatch(false)}
          >
            <Text style={creator.cancelButtonText}>{"Anuluj"}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default PictogramCreator;
