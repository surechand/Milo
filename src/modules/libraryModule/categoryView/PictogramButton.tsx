import React from "react";
import { Text, TouchableOpacity, Image } from "react-native";
import { categoryTab } from "../styles/PictogramDirectory.Styles";
import { Pictogram } from "../../../types/PictogramType";
import { getWordIconsSource } from "../../../assets/index";

interface ButtonProps {
  pictogram: Pictogram;
  onPress: (pictogram: Pictogram) => void;
  onLongPress: (pictogram: Pictogram) => Promise<void>;
}

export const PictogramButton = ({
  pictogram,
  onPress,
  onLongPress,
}: ButtonProps): JSX.Element => (
  <TouchableOpacity
    key={pictogram.id}
    style={categoryTab.pictogramStyle}
    onPress={() => onPress(pictogram)}
    onLongPress={async () => onLongPress(pictogram)}
  >
    <Image
      key={pictogram.filename}
      source={
        pictogram.custom
          ? { uri: pictogram.filename }
          : getWordIconsSource(pictogram.category, pictogram.filename)
      }
      style={categoryTab.thumbnail}
    />
    <Text style={categoryTab.text} adjustsFontSizeToFit>
      {pictogram.word}
    </Text>
  </TouchableOpacity>
);
