/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, FlatList, Modal } from "react-native";
import { categoryTab } from "../styles/PictogramDirectory.Styles";
import { Pictogram, PictogramCategory } from "../../../types/PictogramType";
import { speakWord } from "../../../features/speech/Speech";
import { getIconsData } from "../../../assets/index";
import {
  getDataAsync,
  removePictogramAsync,
} from "../../../features/storage/AsyncStorageHandler";
import { useCallback } from "react";
import { PictogramButton } from "./PictogramButton";
import { styles } from "./CategoryPreview.styles";

interface CategoryPreviewProps {
  selectedCategory: PictogramCategory;
  pictogramSelector: (newWord: Pictogram) => void;
}

const CategoryPreview = ({
  selectedCategory,
  pictogramSelector,
}: CategoryPreviewProps): JSX.Element => {
  const [flatListRowArray, setFlatListRowArray] = useState<
    Pictogram[][] | null
  >(null);
  const categoryKey =
    Object.keys(PictogramCategory)[
      Object.values(PictogramCategory).indexOf(selectedCategory)
    ];
  const [warningModalVisible, setWarningModalVisible] = useState(false);

  const createFlatListRows = async () => {
    const getCustomPictograms = getDataAsync<Pictogram[]>(
      categoryKey.substring(0, 3)
    );
    const customPictogramsArray = await getCustomPictograms();
    let pictogramsArray = getIconsData(selectedCategory);
    if (customPictogramsArray != null) {
      pictogramsArray = [...pictogramsArray, ...customPictogramsArray];
    }
    pictogramsArray.sort((el1, el2) => el1.word.localeCompare(el2.word));
    const rowBuffer = [];
    const lastRowSize = pictogramsArray.length % 3;
    for (let i = 0; i < pictogramsArray.length - lastRowSize; i += 3) {
      rowBuffer.push([
        pictogramsArray[i],
        pictogramsArray[i + 1],
        pictogramsArray[i + 2],
      ]);
    }
    if (lastRowSize == 1) {
      rowBuffer.push([pictogramsArray[pictogramsArray.length - 1]]);
    } else if (lastRowSize == 2) {
      rowBuffer.push([
        pictogramsArray[pictogramsArray.length - 2],
        pictogramsArray[pictogramsArray.length - 1],
      ]);
    }
    return rowBuffer;
  };

  const onPress = (pictogram: Pictogram) => {
    speakWord(pictogram.word);
    pictogramSelector(pictogram);
  };

  const onLongPress = async (pictogram: Pictogram) => {
    if (pictogram.custom) {
      await removePictogramAsync(categoryKey.substring(0, 3), pictogram.id);
      setCategoryData();
    }
    setWarningModalVisible(true);
  };

  const FlatListRow = useCallback(
    (pictogramsRow: { data: Pictogram[] }) => (
      <View style={categoryTab.flatListRow}>
        {pictogramsRow.data.map((pictogram) => (
          <PictogramButton
            key={pictogram.id.concat("buttonkey")}
            pictogram={pictogram}
            onPress={onPress}
            onLongPress={onLongPress}
          />
        ))}
      </View>
    ),
    []
  );

  const setCategoryData = async () => {
    const tempArray = await createFlatListRows();
    setFlatListRowArray(tempArray);
  };

  useEffect(() => {
    setCategoryData();
  }, []);

  return (
    <View style={categoryTab.container}>
      <View style={categoryTab.tabContainer}>
        {flatListRowArray ? (
          <FlatList
            contentContainerStyle={categoryTab.contentContainer}
            data={flatListRowArray}
            keyExtractor={(categoriesRow) => categoriesRow[0].id.concat("row")}
            renderItem={(categoriesRow) => (
              <FlatListRow
                key={categoriesRow.index.toString().concat("flatlistrow")}
                data={categoriesRow.item}
              />
            )}
          />
        ) : (
          <Text style={categoryTab.text} adjustsFontSizeToFit>
            {"Widok kategorii: ".concat(selectedCategory!)}
          </Text>
        )}
      </View>

      <Modal
        animationType="fade"
        transparent={true}
        visible={warningModalVisible}
        onRequestClose={() => {
          setWarningModalVisible(!warningModalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Hello World!</Text>
            <TouchableOpacity
              style={[styles.button, styles.buttonClose]}
              onPress={() => setWarningModalVisible(!warningModalVisible)}
            >
              <Text style={styles.textStyle}>Hide Modal</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default CategoryPreview;
