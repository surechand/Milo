/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React, { useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { library } from "./styles/LibraryView.Styles";
import { Ionicons } from "@expo/vector-icons";
import { CombinedStacksProps } from "../../navigation/NavigationParamLists";
import PictogramCreator from "./PictogramCreator";
import LibraryCategoriesView from "./libraryView/LibraryCategoriesView";

const LibraryView = ({
  route,
  navigation,
}: CombinedStacksProps): JSX.Element => {
  const [creatorEnabled, setCreatorEnabled] = useState<boolean>(false);

  const LibraryBottomRow = () => (
    <View style={library.bottomRow}>
      <TouchableOpacity
        style={library.returnButton}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Ionicons
          name="return-up-back"
          style={library.returnIcon}
          adjustsFontSizeToFit
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={library.addWordButton}
        onPress={() => setCreatorEnabled(true)}
      >
        <Text style={library.addWordButtonText} adjustsFontSizeToFit>
          {"Nowe słowo"}
        </Text>
      </TouchableOpacity>
    </View>
  );

  return (
    <View style={library.container}>
      {creatorEnabled ? (
        <PictogramCreator creatorEnablerDispatch={setCreatorEnabled} />
      ) : (
        <View style={library.container}>
          <LibraryCategoriesView route={route} navigation={navigation} />
          <LibraryBottomRow />
        </View>
      )}
    </View>
  );
};

export default LibraryView;
