import AsyncStorage from "@react-native-async-storage/async-storage";
import { Pictogram } from "../../types/PictogramType";
import { Sentence } from "../../types/SentenceType";

export const storeDataAsync = <Type>(
  storageKey: string
): ((object: Type) => Promise<void>) => {
  const storeData = async (object: Type) => {
    try {
      const jsonValue = await AsyncStorage.getItem("@".concat(storageKey));
      const objectJson = JSON.stringify(object);
      if (jsonValue != null) {
        const mergedJson = jsonValue.concat(",").concat(objectJson);
        await AsyncStorage.setItem("@".concat(storageKey), mergedJson);
      } else {
        await AsyncStorage.setItem("@".concat(storageKey), objectJson);
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  };

  return storeData;
};

export const getDataAsync = <Type>(
  storageKey: string
): (() => Promise<Type>) => {
  const getData = async (): Promise<Type> => {
    try {
      const jsonValue = await AsyncStorage.getItem("@".concat(storageKey));
      return jsonValue != null
        ? JSON.parse("[".concat(jsonValue).concat("]"))
        : null;
    } catch (e) {
      console.log(e);
      throw e;
    }
  };

  return getData;
};

export const removePictogramAsync = async (storageKey: string, id: string) => {
  try {
    const jsonValue = await AsyncStorage.getItem("@".concat(storageKey));
    const pictogramsArray: Pictogram[] =
      jsonValue != null ? JSON.parse("[".concat(jsonValue).concat("]")) : null;
    const filteredArray = pictogramsArray.filter((el) => el.id != id);
    if (filteredArray.length == 0) {
      await AsyncStorage.removeItem("@".concat(storageKey));
    } else {
      const newArray = JSON.stringify(filteredArray);
      const noBracketsString = newArray.slice(1, -1);
      await AsyncStorage.setItem("@".concat(storageKey), noBracketsString);
    }
  } catch (e) {
    console.log(e);
    throw e;
  }
};

export const removeSentenceAsync = async (storageKey: string, id: number) => {
  try {
    const jsonValue = await AsyncStorage.getItem("@".concat(storageKey));
    const pictogramsArray: Sentence[] =
      jsonValue != null ? JSON.parse("[".concat(jsonValue).concat("]")) : null;
    const filteredArray = pictogramsArray.filter((el) => el.id != id);
    if (filteredArray.length == 0) {
      await AsyncStorage.setItem("@".concat(storageKey), "");
    } else {
      const newArray = JSON.stringify(filteredArray);
      const noBracketsString = newArray.slice(1, -1);
      await AsyncStorage.setItem("@".concat(storageKey), noBracketsString);
    }
  } catch (e) {
    console.log(e);
    throw e;
  }
};

export const getItemIndexAsync = async (
  storageKey: string
): Promise<number> => {
  try {
    const lastAddedIndex = await AsyncStorage.getItem(
      "@".concat(storageKey).concat("index")
    );
    if (lastAddedIndex != null) {
      const newIndex = parseInt(lastAddedIndex) + 1;
      await AsyncStorage.setItem(
        "@".concat(storageKey).concat("index"),
        newIndex.toString()
      );
      return newIndex;
    } else {
      await AsyncStorage.setItem("@".concat(storageKey).concat("index"), "0");
      return 0;
    }
  } catch (e) {
    console.log(e);
    throw e;
  }
};

export const clearKey = async (storageKey: string) => {
  try {
    await AsyncStorage.setItem("@".concat(storageKey), "");
  } catch (e) {
    console.log(e);
    throw e;
  }
};

export const clearAllStorage = async () => {
  try {
    await AsyncStorage.clear();
  } catch (e) {
    console.log(e);
    throw e;
  }
};
