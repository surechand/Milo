import * as Speech from "expo-speech";
import { Pictogram } from "../../types/PictogramType";

const config = {
  language: "pl-PL",
  rate: 0.75,
};

export const speakWord = async (word: string) => {
  Speech.speak(word, config);
};

export const speakSentence = async (sentence: Pictogram[]) => {
  const SentenceBuffer: string[] = [];
  sentence.forEach((el) => SentenceBuffer.push(el.word));
  Speech.speak(SentenceBuffer.toString(), config);
};
