**Milo**

**EN**    
Mobile application for verbalizing statements made of pictograms.    
This app has been designed using resources made by [Freepik](https://www.freepik.com) from [Flaticon.com](https://www.flaticon.com/).
    
    
     
**PL**    
Mobilna aplikacja do werbalizacji wypowiedzi zbudowanej z piktogramów.    
Aplikacja powstała przy użyciu materiałów stworzonych przez [Freepik](https://www.freepik.com) ze strony [Flaticon.com](https://www.flaticon.com/).
